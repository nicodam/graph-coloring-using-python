import numpy as np
import matplotlib.pyplot as plt
import datetime
import pytz
import networkx as nx
import csv

def txt_to_csv(file_txt, file_csv) :

    # Open the text file, reading
    with open(file_txt, "r") as txt_file:
        rows = txt_file.readlines()

    # Open the csv file, writing
    with open(file_csv, "w", newline="") as csv_file:
        writer = csv.writer(csv_file, delimiter=",") 
    
        for row in rows:
            row = row.strip()
            elements = row.split()
            writer.writerow(elements)

# Convert the text file to a csv file
def extract_data(file_csv) :
    flights = {}

    # Ouvrir le fichier CSV en mode lecture
    with open(file_csv, "r") as file_csv:
        reader = csv.reader(file_csv)
    
        # Skip the first two rows
        next(reader)
        next(reader)
    
        flights_seen = []

        # Three rows in a row
        for r1, r2, r3 in zip(reader, reader, reader):
            # Extract the import data
            airline = r1[1]
            num_flight = r1[2]
            departure = r2
            arrival = r3
        
            if num_flight in flights_seen : 
                it_flight = 0
                for el in flights_seen :
                    if el.startswith(num_flight) :
                        it_flight += 1
                num_flight = num_flight + str(it_flight)
            
            flights_seen.append(num_flight)
            # Create a dictionnary for the current flight
            flight = {
                "Airline": airline,
                "Departure": departure,
                "Arrival": arrival
            }
        
            # Add the new flight to the 'flights' dictionnary
            flights[num_flight] = flight
    return flights

# Convert the timezones and compute the duration of the flights
def compute_time(flights) :
    cest_timezone = pytz.timezone('Europe/Paris')
    edt_timezone = pytz.timezone('US/Eastern')

    for num_flight, flight in flights.items() :
        # Departure 
        hh_d = int(flight["Departure"][1][0:2])
        mm_d = int(flight["Departure"][1][3:5])
        # Arrival
        hh_a = int(flight["Arrival"][0][0:2])
        mm_a = int(flight["Arrival"][0][3:5])
        
        cest_time_departure = datetime.datetime(2023, 9, 24, hh_d, mm_d, tzinfo=cest_timezone)
        edt_time_departure = cest_time_departure.astimezone(edt_timezone)
        edt_time_arrival = datetime.datetime(2023, 9, 24, hh_a, mm_a, tzinfo=edt_timezone)

        flight["Departure"] = edt_time_departure
        flight["Arrival"] = edt_time_arrival

        duration = edt_time_arrival - edt_time_departure
        flight["Duration"] = duration

# Plot the time periods for the flights
def plot_flights_durations(flights) :
    fig, ax = plt.subplots()
    for i, flight in enumerate(flights) :
        departure = flights[flight]["Departure"]
        arrival = flights[flight]["Arrival"]
        ax.hlines(i+1, departure, arrival, linewidth = 10)
        ax.annotate(f"Flight {i+1}", xy=(arrival, i+1), xytext=(5, 0), textcoords='offset points', fontsize=10, verticalalignment='center')
        ax.annotate(flight, xy=(departure, i+1), xytext=(5, 0), textcoords='offset points', fontsize=10, verticalalignment='center', color = 'white')
    plt.title(f"Time periods for the {len(flights)} flights")
    plt.xlabel('Time')
    plt.ylabel('Flight number')
    plt.grid(True)

# Check if two flights intersect
def intersection(flight1, flight2) :
    d1, a1 = flight1["Departure"], flight1["Arrival"]
    d2, a2 = flight2["Departure"], flight2["Arrival"]
    if a1 <= d2 or a2 <= d1 :
        return True
    else :
        return False

# Plot the time periods for the flights with colors
def plot_inters(flights, index) :
    fig, ax = plt.subplots()
    for i, flight in enumerate(flights) :
        k = list(flights.keys())
        inter = intersection(flights[k[index]], flights[flight])
        if i == index :
            col = 'blue'
        else :
            if inter :
                col = 'green'
            else :
                col = 'red'
        departure = flights[flight]["Departure"]
        arrival = flights[flight]["Arrival"]
        ax.hlines(i+1, departure, arrival, linewidth = 10, color = col)
        ax.annotate(f"Flight {i+1}", xy=(arrival, i+1), xytext=(5, 0), textcoords='offset points', fontsize=10, verticalalignment='center')
        ax.annotate(flight, xy=(departure, i+1), xytext=(5, 0), textcoords='offset points', fontsize=10, verticalalignment='center', color = 'white')
    plt.title(f"Time periods for the {len(flights)} flights")
    plt.xlabel('Time')
    plt.ylabel('Flight number')
    plt.grid(True)

# Create a graph and plot the time periods for the flights
def graph_and_plot(flights):
    # Create graph
    G = nx.Graph()

    # Create edges
    edges = []
    for flight1 in flights:
        for flight2 in flights:
            if not(intersection(flights[flight1], flights[flight2])):
                edges.append((flight1, flight2))
    G.add_edges_from(edges)

    # Degrees for each plane
    degrees = dict(G.degree)
    sorted_degrees = sorted(degrees.keys(), key=lambda x: degrees[x], reverse=True)

    # Levels (or colors)
    levels = {}
    levels_nodes = set()

    # First node
    first_node = sorted_degrees[0]
    levels[first_node] = 1
    levels_nodes.add(first_node)

    for node in sorted_degrees[1:]:
        available_levels = set(range(1, len(levels_nodes) + 2))

        neighbors = G.neighbors(node)
        for neighbor in neighbors:
            if neighbor in levels_nodes:
                if levels[neighbor] in available_levels:
                    available_levels.remove(levels[neighbor])

        if available_levels:
            levels[node] = min(available_levels)
        else:
            max_color = max(levels.values())
            levels[node] = max_color + 1

        levels_nodes.add(node)

    # Color map
    num_levels = max(levels.values())
    color_map = plt.cm.viridis(np.linspace(0, 1, num_levels))

    # Plot the graph
    fig, ax = plt.subplots()
    pos = nx.spring_layout(G)  # Layout du graphe
    node_colors = [color_map[levels[node] - 1] for node in G.nodes()] # attribute a color to each node
    nx.draw(G, pos, with_labels=True, node_color=node_colors, cmap=plt.cm.rainbow)

    # Plot the time periods with colors from levels
    fig, ax = plt.subplots()
    for flight in levels :
        departure = flights[flight]["Departure"]
        arrival = flights[flight]["Arrival"]
        color = color_map[levels[flight] - 1]
        ax.hlines(levels[flight], departure, arrival, linewidth=10, color=color)
        ax.annotate(flight, xy=(departure, levels[flight]), xytext=(5, 0), textcoords='offset points', fontsize=10, verticalalignment='center', color='white')
    plt.title(f"Time periods for the {len(flights)} flights")
    plt.xlabel('Time')
    plt.ylabel('Flight number')
    plt.grid(True)

if __name__ == "__main__":
    flights = extract_data('flights_csv.csv')
    compute_time(flights)
    plot_flights_durations(flights)
    plot_inters(flights, 4)
    graph_and_plot(flights)
    plt.show()