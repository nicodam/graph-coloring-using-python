# Flight Data Analysis

## Overview
This Python script analyzes flight data provided in a text file format. It converts the data into a more manageable CSV format, extracts relevant flight information, computes flight times, and detects intersections between flights. The script also generates graphical representations of flight durations and relationships between flights.

## Architecture

### txt_to_csv
This function reads a text file containing flight data and converts it into a CSV format. Each line in the text file represents a flight, and the function splits the lines into elements and writes them into a CSV file.

```python
def txt_to_csv(file_txt, file_csv) :

    # Open the text file, reading
    with open(file_txt, "r") as txt_file:
        rows = txt_file.readlines()

    # Open the csv file, writing
    with open(file_csv, "w", newline="") as csv_file:
        writer = csv.writer(csv_file, delimiter=",") 
    
        for row in rows:
            row = row.strip()
            elements = row.split()
            writer.writerow(elements)
```

### extract_data
The `extract_data` function extracts flight data from a CSV file. It reads the CSV file and processes the data, creating a dictionary where each flight is represented by its flight number and associated details such as airline, departure time, and arrival time.

```python
def extract_data(file_csv) :
    flights = {}

    # Ouvrir le fichier CSV en mode lecture
    with open(file_csv, "r") as file_csv:
        reader = csv.reader(file_csv)
    
        # Skip the first two rows
        next(reader)
        next(reader)
    
        flights_seen = []

        # Three rows in a row
        for r1, r2, r3 in zip(reader, reader, reader):
            # Extract the import data
            airline = r1[1]
            num_flight = r1[2]
            departure = r2
            arrival = r3
        
            if num_flight in flights_seen : 
                it_flight = 0
                for el in flights_seen :
                    if el.startswith(num_flight) :
                        it_flight += 1
                num_flight = num_flight + str(it_flight)
            
            flights_seen.append(num_flight)
            # Create a dictionnary for the current flight
            flight = {
                "Airline": airline,
                "Departure": departure,
                "Arrival": arrival
            }
        
            # Add the new flight to the 'flights' dictionnary
            flights[num_flight] = flight
    return flights
```

### compute_time
The `compute_time` function converts time zones and computes the duration of each flight. It uses Pytz to convert departure and arrival times from the local time zone to Eastern Daylight Time (EDT) for consistency. The function then calculates the duration of each flight based on the converted times.

```python
def compute_time(flights) :
    cest_timezone = pytz.timezone('Europe/Paris')
    edt_timezone = pytz.timezone('US/Eastern')

    for num_flight, flight in flights.items() :
        # Departure 
        hh_d = int(flight["Departure"][1][0:2])
        mm_d = int(flight["Departure"][1][3:5])
        # Arrival
        hh_a = int(flight["Arrival"][0][0:2])
        mm_a = int(flight["Arrival"][0][3:5])
        
        cest_time_departure = datetime.datetime(2023, 9, 24, hh_d, mm_d, tzinfo=cest_timezone)
        edt_time_departure = cest_time_departure.astimezone(edt_timezone)
        edt_time_arrival = datetime.datetime(2023, 9, 24, hh_a, mm_a, tzinfo=edt_timezone)

        flight["Departure"] = edt_time_departure
        flight["Arrival"] = edt_time_arrival

        duration = edt_time_arrival - edt_time_departure
        flight["Duration"] = duration
```

### intersection
The `intersection` function checks if two flights intersect based on their departure and arrival times. If the arrival time of one flight is before the departure time of the other, they do not intersect. Otherwise, they intersect.

```python
def intersection(flight1, flight2) :
    d1, a1 = flight1["Departure"], flight1["Arrival"]
    d2, a2 = flight2["Departure"], flight2["Arrival"]
    if a1 <= d2 or a2 <= d1 :
        return True
    else :
        return False
```

## Usage
To use this script, ensure you have Python installed on your system along with the required libraries specified in the imports at the beginning of the script. You can run the script by executing it in a Python environment. Make sure to provide the necessary input files containing flight data in text or CSV format. Such inputs are provided in this repository.

## Dependencies
- Python 3.x
- NumPy
- Matplotlib
- Pytz
- NetworkX
- CSV
